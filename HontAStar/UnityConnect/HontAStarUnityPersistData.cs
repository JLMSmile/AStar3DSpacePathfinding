﻿using UnityEngine;
using System;
using System.Collections;

namespace Hont.AStar
{
    public class HontAStarUnityPersistData : ScriptableObject
    {
        [Serializable]
        public class Node
        {
            public int X;
            public int Y;
            public int Z;
            public int Cost;
            public int Mask;
            public bool IsWalkable;
            public Vector3 MappingPos;
            public Vector3 CustomMappingLocalPos;
        }

        public Node[] nodeArr;
        public int lenght;
        public int width;
        public int height;
        public float mappingLengthSize;
        public float mappingWidthSize;
        public float mappingHeightSize;
    }
}
